var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  var naslov = req.query.naslov;
  var povzetek = req.query.povzetek;
  var kategorija = req.query.kategorija;
  var postnaStevilka = req.query.postnaStevilka;
  var kraj = req.query.kraj;
  var povezava = req.query.povezava;
  
  
  
  if(naslov == undefined || naslov.length == 0 || povzetek == undefined  || povzetek.length == 0 || kategorija == undefined || kategorija.length == 0 || postnaStevilka == undefined || postnaStevilka.length == 0 ||  kraj == undefined || kraj.length == 0|| povezava == undefined || povezava.length == 0)
  {
    res.send('Napaka pri vnosu novice!');
  }
  else
  {
    //get id
    var id = 0;
    for(var i in noviceSpomin)
    {
      if(noviceSpomin[i].id > id)
      {
        id = noviceSpomin[i].id;
      }
    }
    
    id++;
    
    //dodaj novico
    noviceSpomin.push({
				id: id,
        naslov: naslov,
        povzetek: povzetek,
        kategorija: kategorija,
        postnaStevilka: postnaStevilka,
        kraj: kraj,
        povezava: povezava
			});
    
    //redirect
    res.redirect('/');
    
  }
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
  
  var novicaId = req.query.id;
  var obstaja = false;
  
  for(var i in noviceSpomin)
  {
    if(noviceSpomin[i].id == novicaId)
    {
      noviceSpomin.splice(i,1);
      obstaja = true;
      break;
    }
  }
  
  if(obstaja == true)
  {
    res.redirect('/');
  }
  else
  {
    res.send('"Novica z id-jem '+ novicaId +' ne obstaja.<br/><a href="javascript:window.history.back()">Nazaj</a>"');
  }
  
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
